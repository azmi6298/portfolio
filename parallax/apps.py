from django.apps import AppConfig


class PaparallaxConfig(AppConfig):
    name = 'paparallax'
